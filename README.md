# On Cross-Domain Transfer in Venue Recommendation

This is our implementation for the paper:

On Cross-Domain Transfer in Venue Recommendation. Jarana Manotumruksa, Dimitrios Rafailidis, Craig Macdonald and Iadh Ounis. In Proceedings of ECIR 2019, Cologne, Germany. April 14-18, 2019

**Please cite our ECIR'19 paper if you use our codes. Thanks!** 

## Environment Settings
We use Keras with Theano as the backend. 
*	Keras version:  '1.2.2'
*	Theano version: '0.9.0'
